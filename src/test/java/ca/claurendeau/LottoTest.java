package ca.claurendeau;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.contains;

import org.junit.BeforeClass;
import org.junit.Test;

import com.eclipsesource.json.JsonObject;

import io.restassured.RestAssured;

public class LottoTest {

    @BeforeClass
    public static void setup() {
        DemowsApplication.main(new String[] {});
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 8089;
    }
    
    @Test
    public void showJsonObject() {
        System.out.println(withJsonContaining("Un user", "password", "un commentaire"));
    }

    @Test
    public void testGetLottoWinners() {
        RestAssured.given()
        .when()
        .get("/lottowinners")
        .then()
        .statusCode(200)
        .body("id", equalTo(1))
        .and()
        .body("winners.winnerId", contains(1,2))
        .and()
        .body("winners.numbers[0]", contains(1,2,3,4,5));
    }
    
    @Test
    public void testPostNewCat() {
        RestAssured.given()
        .when()
        .post("/monurl/cat/1/uneMechanteBete")
        .then()
        .statusCode(201)
        .header("Location", containsString("/monurl/cat/"))
        .body("animalName", equalTo("uneMechanteBete"));
    }
    
    private String withJsonContaining(String name, String password, String comment) {
        return new JsonObject()
                .add("name", name)
                .add("password", password)
                .add("comment", comment)
                .toString();
    }

}
