package ca.claurendeau.domaine;

public class Winner {
    private long winnerId;
    private int[] numbers;
    
    public Winner(long winnerId, int[] numbers) {
        super();
        this.winnerId = winnerId;
        this.numbers = numbers;
    }

    public long getWinnerId() {
        return winnerId;
    }

    public void setWinnerId(long winnerId) {
        this.winnerId = winnerId;
    }

    public int[] getNumbers() {
        return numbers;
    }

    public void setNumbers(int[] numbers) {
        this.numbers = numbers;
    }
    
    
}
