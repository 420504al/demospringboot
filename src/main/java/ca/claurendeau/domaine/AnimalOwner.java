package ca.claurendeau.domaine;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="animal_owner")
public class AnimalOwner {
    
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id", nullable = false, updatable = false)
    @Id
    private Long id = -1l;
    
    @Column(name="firstname")
    private String firstName;
    
    @Column(name="lastname")
    private String lastName;
    
    @OneToMany(mappedBy = "animalOwner")
    @JsonIgnore
    private List<Animal> animals = new ArrayList<>();
    
    public AnimalOwner() {}
    
    public AnimalOwner(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }


    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public void addAnimal(Animal animal) {
        animals.add(animal);
        animal.setAnimalOwner(this);
    }

    public List<Animal> getAnimals() {
        return animals;
    }

    @Override
    public String toString() {
        return "AnimalOwner [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + "]";
    }

}
