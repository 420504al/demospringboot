package ca.claurendeau.domaine;

public class Lotto {
    private long id;
    private int[] winningNumbers  ;
    private Winner[] winners;
    
    public Lotto(long id, int[] winningNumbers, Winner[] winners) {
        this.id = id;
        this.winningNumbers = winningNumbers;
        this.winners = winners;
    }
    
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public int[] getWinningNumbers() {
        return winningNumbers;
    }
    public void setWinningNumbers(int[] winningNumbers) {
        this.winningNumbers = winningNumbers;
    }
    public Winner[] getWinners() {
        return winners;
    }
    public void setWinners(Winner[] winners) {
        this.winners = winners;
    }
    
    public static Lotto getDummyLottoWinners() {
        Winner winner1 = new Winner(1l, new int[] {1,2,3,4,5});
        Winner winner2 = new Winner(2l, new int[] {1,2,3,4,5});
        Lotto lotto = new Lotto(1l, new int[] {1,2,3,4,5}, new Winner[] {winner1, winner2});
        return lotto;
    }

}
