package ca.claurendeau.domaine;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "Dog")
public class Dog extends Animal {
    
    public Dog() {}

    public Dog(String animalName) {
        this.animalName = animalName;
    }

}
