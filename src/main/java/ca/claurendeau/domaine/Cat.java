package ca.claurendeau.domaine;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "Cat")
public class Cat extends Animal {
    
    public Cat() {}

    public Cat(String animalName) {
        this.animalName = animalName;
    }

    @Override
    public String toString() {
        return "Cat [animalName=" + animalName + ", animalOwner=" + animalOwner + "]";
    }

}
