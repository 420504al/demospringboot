package ca.claurendeau.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ca.claurendeau.domaine.Animal;
import ca.claurendeau.domaine.AnimalOwner;
import ca.claurendeau.repository.AnimalOwnerRepository;

public class AnimalCRUD {
    
    @Autowired
    private AnimalOwnerRepository animalOwnerRepository;
    
    public void assignNewDogToAnimalOwner(String lastName, String firstName, String animalName) {
        List<AnimalOwner> animalOwner = animalOwnerRepository.findByLastNameAndFirstName(lastName, firstName);
        if (!animalOwner.isEmpty() ) {
            Animal.createDog(animalName, animalOwner.get(0));
        }
    }

}
