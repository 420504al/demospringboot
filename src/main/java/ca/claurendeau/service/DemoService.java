package ca.claurendeau.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ca.claurendeau.DemowsApplication;
import ca.claurendeau.domaine.Animal;
import ca.claurendeau.domaine.AnimalOwner;
import ca.claurendeau.domaine.Cat;
import ca.claurendeau.domaine.Dog;
import ca.claurendeau.repository.AnimalOwnerRepository;
import ca.claurendeau.repository.CatRepository;
import ca.claurendeau.repository.DogRepository;

@Service
public class DemoService {
    
    private static final Logger log = LoggerFactory.getLogger(DemowsApplication.class);
    
    private AnimalOwnerRepository repository;
    private CatRepository catRepo;
    private DogRepository dogRepo;
    
    public DemoService(AnimalOwnerRepository repository, CatRepository catRepo,
            DogRepository dogRepo) {
            this.repository = repository;
            this.catRepo = catRepo;
            this.dogRepo = dogRepo;
    }
    
    @Transactional
    public void demo() {
     // save a couple of Animal owner
        repository.save(new AnimalOwner("Jack", "Bauer"));
        repository.save(new AnimalOwner("Chloe", "O'Brian"));
        repository.save(new AnimalOwner("Kim", "Bauer"));
        repository.save(new AnimalOwner("David", "Palmer"));
        repository.save(new AnimalOwner("Michelle", "Dessler"));

        // fetch all Animal owners
        log.info("Animal owner found with findAll():");
        log.info("-------------------------------");
        for (AnimalOwner animalOwner : repository.findAll()) {
            log.info(animalOwner.toString());
        }
        log.info("");

        // fetch an individual customer by ID
        Optional<AnimalOwner> animalOwnerOpt = repository.findById(1L);
        AnimalOwner animalOwner = animalOwnerOpt.get();
        log.info("Animal owner found with findOne(1L):");
        log.info("--------------------------------");
        log.info(animalOwner.toString());
        log.info("");

        // fetch customers by last name
        log.info("Animal owner found with findByLastName('Bauer'):");
        log.info("--------------------------------------------");
        for (AnimalOwner bauer : repository.findByLastName("Bauer")) {
            log.info(bauer.toString());
        }
        log.info("");
        
        // Create some animals
        Cat cat = Animal.createCat("la bête", animalOwner);
        catRepo.save(cat);
        
        Dog dog = Animal.createDog("méchant Boris", animalOwner);
        dogRepo.save(dog);
        
        animalOwner.addAnimal(cat);
        animalOwner.addAnimal(dog);
        repository.save(animalOwner);
        
        log.info("" + catRepo.findByAnimalName("la bête"));
        
        log.info("Il y a " + catRepo.count() + " chat(s)");
        
    }
    
    @Transactional
    public void demo2() {
        
    }

}
