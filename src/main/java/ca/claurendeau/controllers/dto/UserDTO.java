package ca.claurendeau.controllers.dto;

import java.util.ArrayList;
import java.util.List;

import ca.claurendeau.domaine.AnimalOwner;

public class UserDTO {
    
    private List<AnimalOwner> animalOwners = new ArrayList<>();

    public UserDTO(List<AnimalOwner> animalOwners) {
        this.animalOwners = animalOwners;
    }

    public List<AnimalOwner> getAnimalOwners() {
        return animalOwners;
    }

}
