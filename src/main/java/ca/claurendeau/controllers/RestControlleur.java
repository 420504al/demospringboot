package ca.claurendeau.controllers;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ca.claurendeau.controllers.dto.UserDTO;
import ca.claurendeau.domaine.Animal;
import ca.claurendeau.domaine.AnimalOwner;
import ca.claurendeau.domaine.Cat;
import ca.claurendeau.domaine.Greeting;
import ca.claurendeau.domaine.Lotto;
import ca.claurendeau.repository.AnimalOwnerRepository;
import ca.claurendeau.repository.CatRepository;

@RestController
public class RestControlleur {
    
    Logger log = LoggerFactory.getLogger(RestControlleur.class);
    
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();
    
    @Autowired
    private AnimalOwnerRepository animalOwnerRepository; 
    
    @Autowired
    private CatRepository catRepository;
    
    @GetMapping(value = "/monurl/{lastName}/{userName}")
    public @ResponseBody UserDTO monUrl(@PathVariable String lastName, @PathVariable String userName) {
        log.info("params : " + lastName + " param2 : " + userName);
        List<AnimalOwner> animalOwners = animalOwnerRepository.findByLastName(lastName);
        return new UserDTO(animalOwners);
    }
    
    @PostMapping(value="/monurl/cat/{animalOwnerId}/{animalName}")
    public ResponseEntity<Cat> createAnimal(@PathVariable Long animalOwnerId, @PathVariable String animalName) {
        Optional<AnimalOwner> findById = animalOwnerRepository.findById(animalOwnerId);
        if (findById.isPresent()) {
            Cat cat = new Cat(animalName);
            cat.setAnimalOwner(findById.get());
            cat = catRepository.save(cat);
            return ResponseEntity
                    .created(getURI("/monurl/cat/" + cat.getId()))
                    .body(cat);
        }
        return null;
    }

    private URI getURI(String uriToBuild) {
        URI uri = null;
        try {
            uri = new URI(uriToBuild);
        } catch (URISyntaxException e) {
            return null;
        }
        return uri;
    }
    
//    @GetMapping(value = "/monurl/{lastName}/{userName}")
//    public @ResponseBody UserWithAnimalDTO animalsForOwner(@PathVariable String lastName, @PathVariable String userName) {
//        log.info("params : " + lastName + " param2 : " + userName);
//        List<AnimalOwner> animalOwners = animalOwnerRepository.findByLastNameAndFirstName(lastName, userName);
//        return getUserWithAnimalsDTOs(animalOwners);
//    }
//
//    private UserWithAnimalDTO getUserWithAnimalsDTOs(List<AnimalOwner> animalOwners) {
//        UserWithAnimalDTO userAnimals = new UserWithAnimalDTO();
//        for (Animal animal : owner.getAnimals()) {
//            
//        }
//        return userAnimals;
//
//    }
   
    @GetMapping("/greeting")
    public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return new Greeting(counter.incrementAndGet(),
                            String.format(template, name));
    }
    
    @GetMapping("/lottowinners")
    public Lotto getLottoWinners() {
        return Lotto.getDummyLottoWinners();
    }

}
