package ca.claurendeau.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ca.claurendeau.domaine.Dog;

public interface DogRepository extends JpaRepository<Dog, Long> {
    

}
